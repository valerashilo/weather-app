//
//  WeatherAppTests.swift
//  WeatherAppTests
//
//  Created by Valera Shilo on 10.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import XCTest
import Combine
import CoreLocation

@testable import WeatherAppSwiftUI

class MockForecastService: ForecastServiceProtocol {
    
    func getCurrentWeather(latitude: Double, longitude: Double) -> AnyPublisher<Forecast, FailureReason> {
        return Just(testForecast)
            .setFailureType(to: FailureReason.self)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    func getFiveDaysForecast(latitude: Double, longitude: Double) -> AnyPublisher<FiveDaysForecastResponse, FailureReason> {
        return Just(testFiveDaysForecast)
            .setFailureType(to: FailureReason.self)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    private var testForecast: Forecast {
        return Forecast()
    }
    
    private var testFiveDaysForecast: FiveDaysForecastResponse {
        return FiveDaysForecastResponse(list: [testForecast])
    }
}

class MockLocationManager: LocationManagerProtocol {
    @Published var coordinate: CLLocationCoordinate2D?
    var coordinatePublished: Published<CLLocationCoordinate2D?> { _coordinate }
    var coordinatePublisher: Published<CLLocationCoordinate2D?>.Publisher { $coordinate }
    
    var targetCoordinate: CLLocationCoordinate2D?
    
    init(targetCoordinate: CLLocationCoordinate2D) {
        self.targetCoordinate = targetCoordinate
    }
    
    func checkLocationStatus() {
        self.coordinate = self.targetCoordinate
    }
}

class HomeVMTests: XCTestCase {
    
    private var mockForecastService: MockForecastService!
    private var subject: HomeVM!
    private var locationManager: MockLocationManager!
    private var cancellables: Set<AnyCancellable> = []
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        
        mockForecastService = MockForecastService()
        locationManager = MockLocationManager(targetCoordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0))
        
        subject = HomeVM(service: mockForecastService, locationManager: locationManager)
    }
    
    func testLoadFiveDayForecasts() throws {
        
        let expectation = XCTestExpectation()
                
        subject.$fiveDaysForecasts
            .dropFirst()
            .sink { forecasts in
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        subject.trigger(.requestLocation)
        
        wait(for: [expectation], timeout: 5)
        
        XCTAssertEqual(subject.fiveDaysForecasts.count, 1)
    }
    
    func testLoadMainForecast() throws {
        
        let expectation = XCTestExpectation()
                
        subject.$mainForecast
            .dropFirst()
            .sink { forecast in
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        subject.trigger(.requestLocation)
        
        wait(for: [expectation], timeout: 5)
        XCTAssertNotNil(subject.mainForecast)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        subject = nil
    }
}
