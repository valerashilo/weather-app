//
//  Double+Extensions.swift
//  WeatherAppSwiftUI
//
//  Created by Valerii Shylo on 03.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import Foundation

extension Double {
    var celsiusDegreesString: String {
        return String(format: "%.0f ℃", self.rounded())
    }
}
