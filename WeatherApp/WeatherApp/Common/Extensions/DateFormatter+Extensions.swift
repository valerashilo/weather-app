//
//  DateFormatter+WACustom.swift
//  WeatherApp
//
//  Created by Valera Shilo on 10.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import Foundation

extension DateFormatter {
    static let forectastTimeFormatter:DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        return formatter
    }()
    
    static let forecastDateFormatter:DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM"
        return formatter
    }()
    
    static let apiDateFormatter:DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier:"UTC")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
}
