//
//  Date+Extensions.swift
//  WeatherAppSwiftUI
//
//  Created by Valerii Shylo on 03.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import Foundation

extension NSDate {
    var dateString : String {
        return DateFormatter.forectastTimeFormatter.string(from: self as Date)
    }
    
    var timeString : String {
        return DateFormatter.forecastDateFormatter.string(from: self as Date)
    }
}
