//
//  UIColor+WACustom.swift
//  WeatherApp
//
//  Created by Valera Shilo on 11.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    open class var lightGray: UIColor {
        get {
            return UIColor(red: CGFloat(233) / 255 , green:CGFloat(233) / 255, blue: CGFloat(242) / 255, alpha: 1)
        }
    }
}
