//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Valerii Shylo on 02.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import Foundation
import CoreLocation


protocol LocationManagerProtocol {
    var coordinate: CLLocationCoordinate2D? { get }
    var coordinatePublished: Published<CLLocationCoordinate2D?> { get }
    var coordinatePublisher: Published<CLLocationCoordinate2D?>.Publisher { get }
    
    func checkLocationStatus()
}

class LocationManager: NSObject, CLLocationManagerDelegate, LocationManagerProtocol {
    @Published var coordinate: CLLocationCoordinate2D?
    var coordinatePublished: Published<CLLocationCoordinate2D?> { _coordinate }
    var coordinatePublisher: Published<CLLocationCoordinate2D?>.Publisher { $coordinate }
    
    @Published var error: Error?
    
    private let locationManager = CLLocationManager()
    
    override init() {
        super.init()
        self.locationManager.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.first?.coordinate {
            self.coordinate = coordinate
        } else {
            self.error = CLError(.locationUnknown)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.error = error
    }
    
    func checkLocationStatus() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.requestLocation()
            
        case .denied:
//            let okButton = UIAlertAction(title: "OK", style: .default, handler: { (action) in
//                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
//                    return
//                }
//
//                if UIApplication.shared.canOpenURL(settingsUrl) {
//                    UIApplication.shared.open(settingsUrl, completionHandler: nil)
//                }
//            })
//
//            self.presentAlert("Error", message: "App needs your location to provide actual forecast. Please go to settings and give us location permission", okButtonAction: okButton)
            break
        default:
            break
        }
        
        return;

    }
}
