//
//  ForecastService.swift
//  WeatherApp
//
//  Created by Valerii Shylo on 02.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import Foundation
import Combine
import CoreData

enum FailureReason : Error {
    case sessionFailed(error: URLError)
    case decodingFailed
    case other(Error)
}

protocol ForecastServiceProtocol {
    func getCurrentWeather(latitude: Double, longitude: Double) -> AnyPublisher<Forecast, FailureReason>
    func getFiveDaysForecast(latitude: Double, longitude: Double) -> AnyPublisher<FiveDaysForecastResponse, FailureReason>
}

class ForecastService: ForecastServiceProtocol {
    
    enum Endpoint {
        case getCurrentWeather
        case getFiveDaysForecast
        
        var value: String {
            switch self {
            case .getCurrentWeather:
                return "weather"
            case .getFiveDaysForecast:
                return "forecast"
            }
        }
    }
    
    private let baseURL = "http://api.openweathermap.org/data/2.5/"
    private let appId = "5f39622c21d57027508ea8e330eda9c1"
    
    let context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
}

extension ForecastService {
    
    func getCurrentWeather(latitude: Double, longitude: Double) -> AnyPublisher<Forecast, FailureReason> {
        
        var urlComponents = URLComponents(string: baseURL.appending(Endpoint.getCurrentWeather.value))
        urlComponents?.queryItems = [URLQueryItem(name: "appId", value: appId),
                                     URLQueryItem(name: "lat", value: "\(latitude)"),
                                     URLQueryItem(name: "lon", value: "\(longitude)"),
                                     URLQueryItem(name: "units", value: "metric")]
        let url = urlComponents!.url!
        
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext] = context
        
        return URLSession.shared.dataTaskPublisher(for: url)
              .map { $0.data }
              .decode(type: Forecast.self, decoder: decoder)
              .mapError({ error in
                switch error {
                case is Swift.DecodingError:
                  return .decodingFailed
                case let urlError as URLError:
                  return .sessionFailed(error: urlError)
                default:
                  return .other(error)
                }
              })
              .receive(on: DispatchQueue.main)
              .eraseToAnyPublisher()
    }
    
    func getFiveDaysForecast(latitude: Double, longitude: Double) -> AnyPublisher<FiveDaysForecastResponse, FailureReason> {
        var urlComponents = URLComponents(string: baseURL.appending(Endpoint.getFiveDaysForecast.value))
        urlComponents?.queryItems = [URLQueryItem(name: "appId", value: appId),
                                     URLQueryItem(name: "lat", value: "\(latitude)"),
                                     URLQueryItem(name: "lon", value: "\(longitude)"),
                                     URLQueryItem(name: "units", value: "metric")]
        let url = urlComponents!.url!
        
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext] = context
        
        return URLSession.shared.dataTaskPublisher(for: url)
              .map { $0.data }
              .decode(type: FiveDaysForecastResponse.self, decoder: decoder)
              .mapError({ error in
                switch error {
                case is Swift.DecodingError:
                  return .decodingFailed
                case let urlError as URLError:
                  return .sessionFailed(error: urlError)
                default:
                  return .other(error)
                }
              })
              .receive(on: DispatchQueue.main)
              .eraseToAnyPublisher()
    }
}
