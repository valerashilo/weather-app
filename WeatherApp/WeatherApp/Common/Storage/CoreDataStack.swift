//
//  WACoreDataStack.swift
//  WeatherApp
//
//  Created by Valera Shilo on 12.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import UIKit
import CoreData
import Foundation

struct CoreDataStackConstants {
    static let modelName = "WeatherApp"
    static let storeFile = "WeatherApp.sqlite"
}

class CoreDataStack: NSObject {
    
    static let defaultCoreDataStack = CoreDataStack()
    
    var managedObjectModel:NSManagedObjectModel?
    var managedObjectContext:NSManagedObjectContext?
    var persistentStoreCoordinator:NSPersistentStoreCoordinator?
    
    fileprivate override init() {
        super.init()
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let storeURL = documentsURL!.appendingPathComponent(CoreDataStackConstants.storeFile)
        let modelURL = Bundle.main.url(forResource: CoreDataStackConstants.modelName, withExtension: "momd")
        self.setupCoreDataStack(NSSQLiteStoreType, modelURL:modelURL!, storeURL:storeURL)
    }
    
    func setupCoreDataStack(_ type:String, modelURL:URL, storeURL:URL) {
        managedObjectModel = NSManagedObjectModel.init(contentsOf: modelURL)
        persistentStoreCoordinator = NSPersistentStoreCoordinator.init(managedObjectModel: managedObjectModel!)
        
        let failureReason = "There was an error creating or loading the application's saved data."
        do {
            try persistentStoreCoordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "ERROR_DOMAIN", code: 9999, userInfo: dict)
            
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
        }
        
        
        managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext!.persistentStoreCoordinator = persistentStoreCoordinator
    }
}
