//
//  WAForecast+CoreDataClass.swift
//  WeatherApp
//
//  Created by Valera Shilo on 12.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import Foundation
import CoreData

enum DecoderConfigurationError: Error {
    case missingManagedObjectContext
}

extension CodingUserInfoKey {
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")!
}

public class Forecast: NSManagedObject, Decodable {
    
    enum CodingKeys: String, CodingKey {
        case weather
        case name
        case main
        case dateTime = "dt"
    }
    
    enum MainCodingKeys: String, CodingKey {
        case temp
    }
    
    required convenience public init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[CodingUserInfoKey.managedObjectContext] as? NSManagedObjectContext else {
            throw DecoderConfigurationError.missingManagedObjectContext
        }
        
        self.init(context: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.cityName = try container.decodeIfPresent(String.self, forKey: .name)
        
        let timestamp = try container.decode(Double.self, forKey: .dateTime)
        self.date = Date.init(timeIntervalSince1970: timestamp) as NSDate
        
        let mainContainer = try container.nestedContainer(keyedBy: MainCodingKeys.self, forKey: .main)
        self.temperature = try mainContainer.decode(Double.self, forKey: .temp)
        
        let weatherArray = try container.decode([Weather].self, forKey: .weather)
        guard let weather = weatherArray.first else {
            return
        }
        
        self.identifier = weather.id
        self.main = weather.main
        self.descriptionString = weather.weatherDescription
        self.imageURLString = weather.icon
    }
}
