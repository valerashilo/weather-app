//
//  Weather.swift
//  WeatherApp
//
//  Created by Valerii Shylo on 02.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import Foundation

struct Weather: Codable {
    let id: Int64
    let main: String
    let weatherDescription: String
    let icon: String

    enum CodingKeys: String, CodingKey {
        case id
        case main
        case weatherDescription = "description"
        case icon
    }
}
