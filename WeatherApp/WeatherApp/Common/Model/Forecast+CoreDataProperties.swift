//
//  WAForecast+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Valera Shilo on 12.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import Foundation
import CoreData


extension Forecast {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Forecast> {
        return NSFetchRequest<Forecast>(entityName: "Forecast")
    }

    @NSManaged public var cityName: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var descriptionString: String?
    @NSManaged public var identifier: Int64
    @NSManaged public var imageURLString: String?
    @NSManaged public var main: String?
    @NSManaged public var temperature: Double

}
