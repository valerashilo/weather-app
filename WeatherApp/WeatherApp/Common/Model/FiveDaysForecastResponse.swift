//
//  FiveDaysForecastResponse.swift
//  WeatherApp
//
//  Created by Valerii Shylo on 02.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import Foundation

struct FiveDaysForecastResponse: Decodable {
    let list: [Forecast]

    enum CodingKeys: String, CodingKey {
        case list
    }
}
