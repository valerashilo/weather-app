//
//  Forecast+Extensions.swift
//  WeatherAppSwiftUI
//
//  Created by Valerii Shylo on 03.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import Foundation

extension Forecast {
    var iconUrl: URL? {
        guard let iconName = imageURLString else {
            return nil
        }
        
        let string = "http://openweathermap.org/img/w/\(iconName).png"
        return URL(string: string)
    }
}
