//
//  WAForecast+Serialization.swift
//  WeatherApp
//
//  Created by Valera Shilo on 12.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import Foundation
import CoreData

extension Forecast {
    
    class func arrayOfForecasts(data: Data) -> [Forecast] {
        
        let context = CoreDataStack.defaultCoreDataStack.managedObjectContext
        let request:NSFetchRequest<Forecast> = Forecast.fetchRequest()
        var results : [Forecast]?
        
        do {
            results = try context?.fetch(request)
            if let results = results {
                for managedObject in results {
                    let managedObjectData:NSManagedObject = managedObject as NSManagedObject
                    context?.delete(managedObjectData)
                }
            }
        } catch let error {
            print("failed to fetch object: \(error)")
        }
        
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext] = context
        
        do {
            let fiveDaysForecastResponse = try decoder.decode(FiveDaysForecastResponse.self, from: data)
            try context?.save()
            return fiveDaysForecastResponse.list
        } catch let error {
            print("Unable to save current weather: \(error)")
        }
        
        return []
    }
    
    class func forecast(data: Data) -> Forecast?{
        let context = CoreDataStack.defaultCoreDataStack.managedObjectContext
        
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext] = context
        
        do {
            let forecast = try decoder.decode(Forecast.self, from: data)
            try context?.save()
            return forecast
        } catch let error {
            print("Unable to save current weather: \(error)")
        }
        
        return nil
    }
    
    class func fetchedForecastForCurrentDate() -> [Forecast] {
        let context = CoreDataStack.defaultCoreDataStack.managedObjectContext
        
        let request:NSFetchRequest<Forecast> = Forecast.fetchRequest()
        request.predicate = NSPredicate(format: "date > %@", NSDate())
        var results : [Forecast] = []
        
        do {
            results = try context?.fetch(request) ?? []
        } catch let error {
            print("failed to fetch forecasts: \(error)")
        }
        
        return results
    }
}
