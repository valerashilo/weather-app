//
//  HomeVM.swift
//  WeatherApp
//
//  Created by Valerii Shylo on 02.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import Foundation
import Combine
import CoreLocation

final class HomeVM: ObservableObject {
    
    enum Input {
        case loadSavedForecasts
        case requestLocation
    }
    
    @Published var mainForecast: Forecast?
    @Published var fiveDaysForecasts: [Forecast] = []
    @Published var showProgress = false
    
    private var service: ForecastServiceProtocol
    private var cancellables = Set<AnyCancellable>()
    private let locationManager: LocationManagerProtocol
    
    init(service: ForecastServiceProtocol, locationManager: LocationManagerProtocol) {
        self.service = service
        self.locationManager = locationManager
        
        locationManager.coordinatePublisher.sink { [weak self] coordinate in
            if let coordinate = coordinate {
                self?.loadForecasts(latitude: coordinate.latitude, longitude: coordinate.longitude)
            } else {
                // TODO: Show alert
            }
        }
        .store(in: &cancellables)
    }
    
    func trigger(_ input: Input) {
        switch input {
        case .loadSavedForecasts:
            let forecasts = Forecast.fetchedForecastForCurrentDate()
            if forecasts.count > 0 {
                mainForecast = forecasts.first
                fiveDaysForecasts = forecasts
            }
        case .requestLocation:
            self.requestLocation()
        }
    }
}

extension HomeVM {
    func requestLocation() {
        locationManager.checkLocationStatus()
    }
    
    func loadForecasts(latitude: Double, longitude: Double) {
        showProgress = true
        
        Publishers.Zip (
            service.getCurrentWeather(latitude: latitude, longitude: longitude),
            service.getFiveDaysForecast(latitude: latitude, longitude: longitude)
        )
        .sink { [weak self] error in
            self?.showProgress = false
        } receiveValue: { [weak self] forecast, fiveDaysForecast in
            self?.showProgress = false
            self?.mainForecast = forecast
            self?.fiveDaysForecasts = fiveDaysForecast.list
        }
        .store(in: &cancellables)
    }
}
