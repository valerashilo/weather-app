//
//  WAPushTransition.swift
//  WeatherApp
//
//  Created by Valera Shilo on 12.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import UIKit

class PushTransition: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.85
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = (transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? HomeViewController),
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? ForecastCollectionViewController else {
                return
        }
        
        let containerView = transitionContext.containerView
        
        let cover = UIView(frame: toVC.view.frame)
        cover.backgroundColor = UIColor.white
        cover.alpha = 0
        
        let initialFrame = fromVC.animationFrame()
        let finalFrame = toVC.selectedCellFrame()
        
        let image = fromVC.animationImage()
        
        
        let snapshot = UIView(frame: initialFrame)
        snapshot.layer.cornerRadius = 5
        snapshot.backgroundColor = UIColor.lightGray
        
        containerView.addSubview(toVC.view)
        toVC.view.isHidden = true
        containerView.addSubview(cover)
        containerView.addSubview(snapshot)
        
        
        let imageView = UIImageView()
        imageView.image = image
        snapshot.addSubview(imageView)
        
        imageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        imageView.centerXAnchor.constraint(equalTo: snapshot.centerXAnchor).isActive = true
        let imageViewCenterYConstraint = imageView.centerYAnchor.constraint(equalTo: snapshot.centerYAnchor, constant: 10)
        
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: 0.15, animations: {
            cover.alpha = 1
        }) { _ in
            toVC.view.isHidden = false
            UIView.animate(withDuration: duration - 0.4, animations: {
                snapshot.frame = finalFrame!
                imageViewCenterYConstraint.constant = 0
                snapshot.layoutIfNeeded()
            }) { _ in
                UIView .animate(withDuration: 0.25, animations: {
                    cover.alpha = 0
                    snapshot.alpha = 0
                }, completion: { _ in
                    snapshot.removeFromSuperview()
                    cover.removeFromSuperview()
                    let cancelled = transitionContext.transitionWasCancelled
                    transitionContext.completeTransition(!cancelled)
                })
            }
        }
    }
}
