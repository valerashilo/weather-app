//
//  WAForecastView.swift
//  WeatherApp
//
//  Created by Valera Shilo on 10.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import UIKit
import Kingfisher

class ForecastView: UIView {
    @IBOutlet weak var mainLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var temperatureLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var timeLabel: UILabel?
    @IBOutlet weak var cityLabel: UILabel?
    
    var forecast: Forecast? {
        didSet {
            guard let forecast = forecast else {
                assert(false, "trying to configure ForecastView with nil forecast")
                return
            }
            
            mainLabel?.text = forecast.main
            temperatureLabel?.text = forecast.temperature.celsiusDegreesString
            timeLabel?.text = forecast.date?.timeString
            dateLabel?.text = forecast.date?.dateString
          
            if let url = forecast.iconUrl {
                imageView?.kf.setImage(with: url)
            }
                        
            descriptionLabel?.text = forecast.descriptionString
            cityLabel?.text = forecast.cityName
        }
    }
}
