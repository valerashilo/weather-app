//
//  WACollectionViewController.swift
//  WeatherApp
//
//  Created by Valera Shilo on 10.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ForecastCell"

class WACollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var forecastView:ForecastView?
    var forecast:Forecast? {
        didSet {
            forecastView?.forecast = forecast
        }
    }
}

class HomeCollectionViewController: UICollectionViewController {
    
    var selectedCellFrame: CGRect?
    var selectedCellImage: UIImage?
    
    var forecasts:Array<Forecast>? {
        didSet {
            self.collectionView?.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: HomeCollectionViewController.self) {
            let controller = segue.destination as! HomeCollectionViewController
            controller.forecasts = self.forecasts
            
            if let selectedCell = sender as? WACollectionViewCell, let _ = selectedCell.forecastView?.frame {
                let fraame = collectionView?.convert(selectedCell.frame, to: self.view)
                selectedCellFrame = fraame
                selectedCellImage = selectedCell.forecastView?.imageView?.image
            }
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if  self.forecasts != nil {
            return self.forecasts!.count
        }
        return 0;
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! WACollectionViewCell
        let forecast = self.forecasts?[indexPath.row]
        cell.forecast = forecast
        return cell
    }
}
