//
//  WAForecastCollectionViewController.swift
//  WeatherApp
//
//  Created by Valera Shilo on 10.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import UIKit

private struct Constants {
    static let collectionCellHeight:CGFloat = 150
}

class ForecastCollectionViewController: HomeCollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var selectedForecast:Forecast?
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width , height: Constants.collectionCellHeight)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "5 days forecast"
    }
    
    func selectedCellFrame() -> CGRect?{

        if let indexPath = selectedForecastIndexPath() {
            self.collectionView?.reloadData()
            self.collectionView?.layoutIfNeeded()
            self.collectionView?.scrollToItem(at: indexPath, at: .top, animated: false)
            
            let cell = self.collectionView?.cellForItem(at: indexPath) as? WACollectionViewCell
            var frame = cell?.forecastView?.frame
            
            frame?.origin.y -= (self.collectionView?.contentOffset.y)!
            
            return frame
        }
        return CGRect.zero
    }
    
    func selectedForecastIndexPath() -> IndexPath? {
        // TODO: Refactor this
        let index = self.forecasts?.firstIndex(where: { (item) -> Bool in
            item.identifier == selectedForecast?.identifier
        })
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        return indexPath
    }
}
