//
//  ViewController.swift
//  WeatherApp
//
//  Created by Valera Shilo on 10.06.17.
//  Copyright © 2017 v8. All rights reserved.
//

import UIKit
import CoreLocation
import Combine

class HomeViewController: UIViewController {
    
    private let viewModel = HomeVM(service: ForecastService(context: CoreDataStack.defaultCoreDataStack.managedObjectContext!), locationManager: LocationManager())
    private var cancellables = Set<AnyCancellable>()
    
    @IBOutlet weak var mainForecastView:ForecastView?
    @IBOutlet weak var containerView:UIView?
    @IBOutlet weak var refreshButton:UIBarButtonItem?
    @IBOutlet weak var emptyPlaceholder:UILabel?
    
    var forecastController: HomeCollectionViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubscriptions()
            
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        NotificationCenter.default.addObserver(self , selector: #selector(handleNotification) , name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.trigger(.requestLocation)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: HomeCollectionViewController.self) {
            self.forecastController = (segue.destination as! HomeCollectionViewController)
        }
    }
    
    // MARK: - Combine
    func addSubscriptions() {
        viewModel.$mainForecast
            .receive(on: DispatchQueue.main)
            .sink { [weak self] forecast in
                guard let forecast = forecast else {
                    self?.emptyPlaceholder?.isHidden = false
                    return
                }
                self?.emptyPlaceholder?.isHidden = true

                self?.mainForecastView?.forecast = forecast
            }
            .store(in: &cancellables)
        
        viewModel.$fiveDaysForecasts
            .receive(on: DispatchQueue.main)
            .sink { [weak self] forecasts in
                self?.forecastController?.forecasts = forecasts
            }
            .store(in: &cancellables)
        
        viewModel.$showProgress
            .receive(on: DispatchQueue.main)
            .sink { [weak self] showProgress in
                self?.navigationItem.title = showProgress ? "Loading..." : "Current Weather"
            }
            .store(in: &cancellables)
    }
    
    
    // MARK: - Custom Actions
    @objc func handleNotification() {
        viewModel.trigger(.requestLocation)
    }

    @IBAction func refreshButtonAction(sender:UIButton) {
        viewModel.trigger(.requestLocation)
    }
    
    // MARK: - Custom Transition Helpers
    func animationFrame() -> CGRect {
        if let frame = self.forecastController?.selectedCellFrame , let containerFrame = self.containerView?.frame{
            let originX = frame.origin.x + containerFrame.origin.x
            let originY = frame.origin.y + containerFrame.origin.y
            return CGRect(x: originX, y: originY, width:  frame.size.width, height: frame.size.height)
        }
        return CGRect.zero
    }
    
    func animationImage() -> UIImage? {
        return self.forecastController?.selectedCellImage
    }
}

extension UIViewController {
    func presentAlert(_ title: String?, message: String? , okButtonAction: UIAlertAction?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let okAction = okButtonAction {
            alertController.addAction(okAction)
        } else {
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(ok)
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    func presentAlert(_ title: String?, message: String?) {
        self.presentAlert(title, message: message, okButtonAction: nil)
    }
}
