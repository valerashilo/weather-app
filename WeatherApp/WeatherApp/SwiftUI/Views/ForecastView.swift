//
//  ForecastView.swift
//  WeatherAppSwiftUI
//
//  Created by Valerii Shylo on 03.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import SwiftUI
import Kingfisher

struct ForecastView: View {
    enum LayoutType {
        case main
        case grid
        case list
    }
    
    var forecast: Forecast
    var layoutType: LayoutType
    
    var body: some View {
        switch layoutType {
        case .main:
            mainLayout
        case .grid:
            gridLayout
        case .list:
            listLayout
        }
    }
}

extension ForecastView {
    var mainLayout: some View {
        VStack(spacing: .zero) {
            Text(forecast.cityName ?? "")
                .font(.headline)
                .frame(height: 50)
                .padding(.top, 20)
            
            Text(forecast.date?.dateString ?? "")
                .font(.callout)
            
            Text(forecast.date?.timeString ?? "")
                .font(.footnote)
            
            HStack(spacing: 10) {
                HStack {
                    Spacer()
                    
                    Text(forecast.temperature.celsiusDegreesString)
                        .font(.title)
                    
                    Spacer()
                }
                
                HStack {
                    Spacer()
                    VStack(spacing: .zero) {
                        if let url = forecast.iconUrl {
                            KFImage(url)
                                .frame(height: 60)
                        }
                        
                        Text(forecast.main ?? "")
                            .font(.callout)
                        Text(forecast.descriptionString ?? "")
                            .font(.footnote)
                    }
                    Spacer()
                }
                .padding(.bottom, 20)
            }
            .padding(.horizontal, 20)
        }
        .background(.gray.opacity(0.5))
        .cornerRadius(20)
        .padding(10)
    }
    
    var gridLayout: some View {
        ZStack {
            VStack(spacing: .zero) {
                Text(forecast.date?.dateString ?? "")
                    .font(.callout)
                
                Text(forecast.date?.timeString ?? "")
                    .font(.footnote)
                
                if let url = forecast.iconUrl {
                    KFImage(url)
                        .frame(height: 40)
                }
                
                Text(forecast.temperature.celsiusDegreesString)
                    .font(.title)
            }
            .padding(10)
        }
        .background(.gray.opacity(0.5))
        .cornerRadius(20)        
    }
    
    var listLayout: some View {
        ZStack {
            HStack(spacing: .zero) {
                Text(forecast.temperature.celsiusDegreesString)
                    .font(.title)
                
                Spacer()
                
                VStack(alignment: .trailing, spacing: 5) {
                    
                    Text(forecast.date?.dateString ?? "")
                        .font(.callout)
                    
                    Text(forecast.date?.timeString ?? "")
                        .font(.footnote)
                    
                    Text(forecast.descriptionString ?? "")
                        .font(.footnote)
                }
            }
            
            HStack {
                Spacer()
                
                if let url = forecast.iconUrl {
                    KFImage(url)
                        .frame(height: 50)
                }
                
                Spacer()
            }
        }
        .padding(10)
    }
}
