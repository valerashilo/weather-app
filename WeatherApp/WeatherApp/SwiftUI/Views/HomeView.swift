//
//  HomeView.swift
//  WeatherApp
//
//  Created by Valerii Shylo on 02.07.2022.
//

import SwiftUI
import Kingfisher

struct HomeView: View {
    
    @ObservedObject var viewModel: HomeVM
    
    var body: some View {
        NavigationView {
            VStack(spacing: .zero) {
                if let forecast = viewModel.mainForecast {
                    ForecastView(forecast: forecast, layoutType: .main)
                }
                
                gridView
                
                Spacer()
            }
            .navigationTitle("Current Weather")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        viewModel.trigger(.requestLocation)
                    } label: {
                        Image(systemName: "arrow.clockwise")
                            .tint(.blue)
                    }
                }
            }
            .onAppear() {
                viewModel.trigger(.loadSavedForecasts)
                viewModel.trigger(.requestLocation)
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

extension HomeView {
    var gridView: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHGrid(rows: [GridItem(.fixed(80))], spacing: 10) {
                ForEach(viewModel.fiveDaysForecasts, id: \.date?.description) { forecast in
                    NavigationLink {
                        ForecastListView(forecasts: viewModel.fiveDaysForecasts)
                    } label: {
                        ForecastView(forecast: forecast, layoutType: .grid)
                    }
                }
            }
            .padding(.horizontal, 10)
        }
        .frame(height: 150)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(viewModel: HomeVM(service: ForecastService(context: CoreDataStack.defaultCoreDataStack.managedObjectContext!), locationManager: LocationManager()))
    }
}
