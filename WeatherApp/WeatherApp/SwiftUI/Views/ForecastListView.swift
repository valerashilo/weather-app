//
//  ForecastListView.swift
//  WeatherAppSwiftUI
//
//  Created by Valerii Shylo on 03.07.2022.
//  Copyright © 2022 v8. All rights reserved.
//

import SwiftUI

struct ForecastListView: View {
    
    var forecasts: [Forecast]
    
    var body: some View {
        List {
            ForEach(forecasts, id: \.date?.description) { forecast in
                ForecastView(forecast: forecast, layoutType: .list)
            }
            .listRowBackground(Color.clear)
            .listRowSeparator(.hidden)
            
        }
        .listStyle(.plain)
    }
}
