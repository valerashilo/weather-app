//
//  WeatherApp.swift
//  WeatherApp
//
//  Created by Valerii Shylo on 02.07.2022.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView(viewModel: HomeVM(service: ForecastService(context: CoreDataStack.defaultCoreDataStack.managedObjectContext!), locationManager: LocationManager()))
        }
    }
}
